




function Pokemon(name, level){

    //pokemon properties
    this.name = name;
    this.lvl = level;
    this.health = 2 * level;
    this.atk = level;
    
    //METHODS
    this.tackle = function(target){
        
        console.log(`${this.name} tackled ${target.name}`)
        
        let newhealth = target.health - this.atk;
        target.health = newhealth;
        
        console.log(` ${target.name}'s health is now reduced to  ${newhealth}`);

        return newhealth < 10 ? target.faint() : console.log(`${target.name}' health is still above 10`)

         

    };
    this.faint = function(){
        console.log(`${this.name} fainted.`)
    };
};

let charizard = new Pokemon("Charizard", 10);
let rattata = new Pokemon("Rattata", 2);

console.log(charizard);
console.log(rattata);

//rattata.tackle(charizard);
